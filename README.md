# CL w/Batfish



![](diagrams/topology-diagram.png)


## Verification (aka ensure traffic is flowing as expected)


##### Server01 to Server04 (VXLAN Bridging Inter Rack)
```
cumulus@server01:~$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=2.87 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=6.41 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=2.95 ms
```

#### Server05 to Server06 (VXLAN Routing Intra Rack)
```
cumulus@server05:~$ ping 10.2.25.102
PING 10.2.25.102 (10.2.25.102) 56(84) bytes of data.
64 bytes from 10.2.25.102: icmp_seq=1 ttl=63 time=1.14 ms
64 bytes from 10.2.25.102: icmp_seq=2 ttl=63 time=1.05 ms
64 bytes from 10.2.25.102: icmp_seq=3 ttl=63 time=1.43 ms
```

#### Server03 to Server05 (VXLAN Routing Inter Rack)
```
cumulus@server03:~$ ping 10.2.2.102
PING 10.2.2.102 (10.2.2.102) 56(84) bytes of data.
64 bytes from 10.2.2.102: icmp_seq=1 ttl=62 time=2.69 ms
64 bytes from 10.2.2.102: icmp_seq=2 ttl=62 time=3.03 ms
64 bytes from 10.2.2.102: icmp_seq=3 ttl=62 time=2.35 ms
```


## Current Manual Steps/Gotchas:

1. Install Gitlab Runner and register it with the project
2. "ncluhost" is used as a renderer of configs from a flatfile format to an NCLU model, for Batfish consumption.
3. "server01" is used to host a copy of these NCLU configurations and acts as the gitlab runner for the Batfish portion.
