from pybatfish.client.commands import *
from pybatfish.question.question import load_questions
from pybatfish.datamodel.flow import (HeaderConstraints,PathConstraints)
from pybatfish.question import bfq

NETWORK_NAME = "cl_test"
# bf_delete_network(NETWORK_NAME)

BASE_SNAPSHOT_NAME = "cl_test"
SNAPSHOT_PATH = "/home/user/cldemo2/batfish/"

load_questions()

print("[*] Initializing BASE_SNAPSHOT")
bf_set_network(NETWORK_NAME)
bf_init_snapshot(SNAPSHOT_PATH, name=BASE_SNAPSHOT_NAME, overwrite=True)


bgpSessStat = bfq.bgpSessionStatus().answer().frame()
_bgpSessStat = bgpSessStat[bgpSessStat['Remote_IP'] != '172.17.31.10']

print(_bgpSessStat)

if len(_bgpSessStat[_bgpSessStat['Established_Status'] != 'ESTABLISHED']) > 0:
  raise Exception('Not All BGP Sessions Are Established')
else:
  print("All BGP Sessions Are Good")
